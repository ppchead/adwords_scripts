function getEndedOrPausedAdGroupsCampaigns() {
  var userDateNow = userDate()
  Logger.log(getReportRanges())
  var allCampaigns = AdWordsApp.campaigns()
  .withCondition("Status = ENABLED")
  .forDateRange(getReportRanges())
  .get()
  var ids = []
  while (allCampaigns.hasNext()) {
    var campaign = allCampaigns.next()

    if (endedStatus(campaign, userDateNow)=='ENDED') {
      //      Logger.log('%s is Ended', campaign.getName())
      ids.push(campaign.getId())
    }
  }
  var adGroups = AdWordsApp.adGroups()
  .withCondition("CampaignStatus = ENABLED")
  .forDateRange(getReportRanges())
  .get()
  var allPausedGroups = {}
  while (adGroups.hasNext()) {
    var adGroup = adGroups.next()
    var campaignId = adGroup.getCampaign().getId().toString()
    if (typeof allPausedGroups[campaignId] == 'undefined') allPausedGroups[campaignId] = true
    if (!adGroup.isRemoved()) {
      allPausedGroups[campaignId] = allPausedGroups[campaignId] && adGroup.isPaused()
    }
  }
  var allPaused =[]
  var allCampaignsIds =  Object.keys(allPausedGroups)
  for (var i in allCampaignsIds) {
    var id = allCampaignsIds[i]
    if (allPausedGroups[id]) allPaused.push(id)
  }
  return ids.concat(allPaused)
}

var Results = function () {
//  this.keys = []
}

Results.prototype.create = function (id, period, metrics) {
  this[id] = {}
  this.id = id
  this[id][period] = {}
  for (var key in metrics) {
    this[id][period][metrics[key]] = 0
  }
}

Results.prototype.set = function (id, period, metrics) {
  if (!this[id]) {
    this.create(id, period, metrics)
  } else {
    if (!this[id][period]) this[id][period] = {}
    for (var key in metrics) {
      this[id][period][metrics[key]] = 0
    }
  }
}

Results.prototype.dataRow = function (metrics, placeholder, period) {
  var self = this
  var na = placeholder || '--'
  var print = function (id) {
    var results = []
    var periods = (period) ? [period] : Object.keys(self[id]).filter(function (key) {return key.match('period')})
    for (var j in periods) {
      var result = []
      var converted = convertMetrics(metrics)
      for (var i in converted) {
        var metric = converted[i]
        if (converted[i] == 'Anomaly') {
          try {
          if (self[id].declining.flag || self[id].historical.flag ) {
            result.push(['declining' ].concat((self[id].declining.metrics||['-']),
                                              [' historical'], (self[id].historical.metrics||['-'])).join())
          } else {
            result.push('tbd')
          }
          } catch (e) {
            Logger.log(id)
            Logger.log(self[id])
            throw Error(e)
          }
        } else {

          try {
            if (typeof self[id][periods[j]][metric] != 'undefined' && self[id][periods[j]][metric] != null ) {
              result.push(self[id][periods[j]][metric])
            } else {
              result.push(na)
            }
          } catch (e) {
            Logger.log(self[id])
            throw Error(e)
          }
        }
      }
      results.push(result)
    }
    return results
  }
  return print
}

Results.prototype.loader = function (metrics) {
  var self = this
  var load = function (id, row) {
    if (!self[id]) self.set(id, row.Period, metrics)
    if (!self[id][row.Period]) self.set(id, row.Period, metrics)
    for (var key in metrics) {
      self[id][row.Period][metrics[key]] = row[metrics[key]]
    }
  }
  return load
}

Results.prototype.additiveLoader = function (metrics) {
  var self = this
  var load = function (id, row) {
    if (!self[id]) self.set(id, row.Period, metrics)
    if (!self[id][row.Period]) self[id][row.Period] = {}
    for (var key in metrics) {
      if (!self[id][row.Period][metrics[key]]) self[id][row.Period][metrics[key]] = 0
      self[id][row.Period][metrics[key]] += parseFloat(row[metrics[key]])
    }
  }
  return load
}

Results.prototype.weigthedLoader = function (valueKey, weightKey) {
  var self = this
  var load = function (id, row) {
    if (!self[id] || !self[id][row.Period]) {
      self.set(id, row.Period, [valueKey, weightKey,valueKey+'Weighted '+weightKey])
    }
    var metrics = [valueKey, weightKey, (valueKey+'Weighted '+weightKey)]
    metrics.map(function(metricKey) {
      if (!self[id][row.Period][metricKey]) self[id][row.Period][metricKey] = 0
    })
    var value = parseFloat(row[valueKey])
    if (!isNaN(value)) {
      self[id][row.Period][valueKey+'Weighted '+weightKey] += value*parseFloat(row[weightKey])
      self[id][row.Period][valueKey] = value
    } else {
      self[id][row.Period][valueKey] = row[valueKey]
    }
    self[id][row.Period][weightKey] += parseFloat(row[weightKey])
  }
  return load
}

Results.prototype.weigthKeys = function (valueKey, weightKey) {
  var ids = Object.keys(this)
  for (var i in ids) {
    var resultGroup = this[ids[i]]
    var periods = Object.keys(resultGroup).filter(function (key) {return key.match('period')})
    for (var j in periods) {
      var result = resultGroup[periods[j]]
      if (result[valueKey+'Weighted '+weightKey] && result[weightKey]) {
        result['Weighted '+valueKey] = result[valueKey+'Weighted '+weightKey] / result[weightKey]
      } else {
        result['Weighted '+valueKey] = 0
      }
    }
  }
}

Results.prototype.each = function (parameterFunction) {
  var ids = Object.keys(this)
  for (var i in ids) {
    parameterFunction(this[ids[i]])
  }
}

Results.prototype.filterById = function (idFilterFunction, parameterFunction) {
  var ids = Object.keys(this)
  for (var i in ids) {
    if (idFilterFunction(ids[i])) {
      parameterFunction(this[ids[i]])
    }
  }
}

Results.prototype.head = function (n) {
  var ids = Object.keys(this)
  for (var k = 0; k < n; k++) {
    Logger.log('at %s: object: %s', ids[k], this[ids[k]])
  }
}
